<%@ page contentType="text/html;charset=UTF-8" isELIgnored="true"
	language="java"%>
<%@ page import="com.google.appengine.api.users.User"%>
<%@ page import="com.google.appengine.api.users.UserService"%>
<%@ page import="com.google.appengine.api.users.UserServiceFactory"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<title>SymjaWeb - Computer Algebra system</title>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquerymobile/1.4.5/jquery.mobile.min.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="DESCRIPTION"
	content="SymjaWeb computer algebra tool for mobile phones">
<meta name="KEYWORDS"
	content="Mathematics, pda, iPhone, mobile phone, ajax, front end, java, computer algebra, symbolic math">

<style type="text/css"> 
#result { display: block; border: 3px solid #38c; padding: 5px; }
pre { min-height: 32px; overflow: auto; overflow: scroll -moz-scrollbars-horizontal; }
#console { background-color:white; }
</style>

<script type="text/javascript" language="javascript">
var handlerFunc = function(btnStr, resultstr) {  
  var index1 = resultstr.indexOf(';'); 
  if (index1>=0) { 
    var index2 = resultstr.indexOf(';',index1+1);
	if (index2>=0) { 
      var rType = resultstr.substring(index1+1, index2);
      if (rType=='error') {
        resultstr = resultstr.substring(index2+1).replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;");
    	resultstr = '<span style="color:red; display: block;">Error:\n'+resultstr+'</span>'; 
    	$('#result').html(resultstr);
      } else {
        $('#result').html("");
	    resultstr = resultstr.substring(index2+1);
	    $('#console').prepend("<pre>&gt;&gt;&gt; "+btnStr+"<br />"+resultstr.replace(/&/g,"&amp;").replace(/</g,"&lt;").replace(/>/g,"&gt;")+"</pre>");
      }  
	  return;
	}
  }
  document.getElementById('result').innerHTML = 'Error:\n no connection or wrong response format.';        
}
var errFunc = function(t) {
  document.getElementById('result').innerHTML = 'Error ' + t.status + ' -- ' + t.statusText;   
}
 
function getResult(nameArray, btnStr, mode, func) {
  $( "#panel" ).panel( "close" );
  var temp;
  for (var i = 0; i < nameArray.length; ++i) {
	temp = document.getElementById(nameArray[i]).value;
	btnStr = btnStr.replace(new RegExp(nameArray[i],"g"), temp);
  } 
  document.getElementById('result').innerHTML = 'Loading...';
  var poststr = "evaluate=" + encodeURIComponent( btnStr )+"&mode="+mode+"&function="+func;	  
  $.ajax({ type: "POST", url: 'calc', data: poststr, 
    success: function(responseText){handlerFunc(btnStr, responseText) },
    error: function(responseText){errFunc(responseText) }
  }); 
  document.getElementById("ta").focus();
}
function close() { $('#result').html(""); }
function clearta(nameArray) {
  for (var i = 0; i < nameArray.length; ++i) {
    document.getElementById(nameArray[i]).value = '';
  } 
  if (nameArray.length>0) {
	document.getElementById(nameArray[0]).focus();
  }
  $('#result').html("");
}
function clearall(nameArray) {
  clearta(nameArray);
  $('#result').html("");
  $('#console').html("");
} 
function help() {
  $( "#panel" ).panel( "close" );
  $('#result').html(
   '<p>Show an <a href="media/doc/functions.html" target="_blank">introducton and reference of functions for the Symja language</a> in a new browser tab.</p>'+
   '<h2>Common evaluation buttons</h2><p>The following buttons are always defined in the user interface:'+
   '<ul><li><strong>==</strong> - symbolic evaluation </li>'+
   '<li><strong>&asymp;&asymp;</strong> - numeric evaluation </li>'+
   '<li><strong>CL</strong> - clears the input text area </li>'+
   '<li><strong>CA</strong> - clears the input and output text area</li>'+
   '<li><strong>Link</strong> - creates a link from the input text which you can share with your friends</li>'+
   '</ul></p><h2>Persisting formulas</a></h2><p>Through the <strong>Login</strong> link users can sign in with their Google User Account.'+
   ' When logged in, it&#x27;s possible to persist user defined formulas and constants with the so-called <i>$ user symbols</i> in the Google App Engine datastore. A special user variable is the <tt>$ans</tt> variable, which returns the answer expression from the last computation.</p>'+
   '<p>You can for example create your own rules or formulas for a new defined symbol <tt>$cube</tt> with the <tt>:=</tt> operator. Type the following rules in the input area and press the <strong>Sym</strong> button to define your rules:'+
   ' </p><pre class="prettyprint"> SetAttributes[$cube,Listable];'+
   '$cube[x_]:=x^3</pre><p>Now you can use this rule by evaluating a symbolic argument </p><pre class="prettyprint"> $cube[a] </pre><p>and you get the result <tt>a^3</tt> in the output area.</p>'+
   '<p>Because the <tt>$cube</tt> symbol has the attribute <tt>Listable</tt> you can also evaluate a list of numbers like this </p><pre class="prettyprint"> $cube[{1,2,3,4,5}] </pre><p>and you get the result <tt>{1,8,27,64,125</tt>}.</p>'+
   '<p>In the same way you can define constants with the <tt>=</tt> operator: </p><pre class="prettyprint"> $theAnswerToAllQuestions = 42 </pre><p>By evaluating </p>'+
   '<pre class="prettyprint"> $theAnswerToAllQuestions </pre>'+
   '<p>you get the defined value <tt>42</tt>. </p>'+
   '<p>If you are logged in again at a later date, you can reuse the <i>$ user symbols</i> without redefining them. </p>'+
   '<h2>Buttons for logged in users</a></h2><p>The following buttons are <strong>only</strong> defined in the user interface, if you are logged in with your Google Account: <ul><li><strong>Def</strong> - shows the definition of the <i>$ user symbol</i> that the user has typed into the input area (i.e. the <i>$ user symbol</i> <strong>$cube</strong> used in the example above, will show the defined rules for <strong>$cube</strong> in the output area). </li><li><strong>Vars</strong> - shows all defined user variables from the Google App Engine datastore. At the moment a user can define up to 100 <i>$ user symbol</i> formulas.');
}
function linkta() {
  $( "#panel" ).panel( "close" );
  var tmp, resultstr;
  tmp = document.getElementById("ta").value;	
  resultstr = "http://symjaweb.appspot.com/?ci=ta:Input:t:"+encodeURIComponent(tmp); 
  document.getElementById('result').innerHTML = resultstr;
}
function ol(){document.getElementById("ta").focus();}
</script>
</head>

<body onload="ol()">
<div data-role="page" class="type-index">

<div style="margin: 0px; padding: 0px; overflow:hidden;"  data-role="header">

<div data-role="controlgroup" data-type="horizontal" data-mini="true"> 
    <a href="#panel" class="ui-btn ui-btn-icon-left ui-icon-bars">&thinsp;</a>
    <a class="ui-btn ui-btn-inline" type="submit" title="Evaluate input in symbolic mode" onclick="getResult(new Array('ta'),'ta','',''); return false;" >==</a>
    <a class="ui-btn ui-btn-inline" type="submit" title="Evaluate input in numeric mode" onclick="getResult(new Array('ta'),'ta','N',''); return false;" >&asymp;&asymp;</a>
    <a class="ui-btn ui-btn-inline" type="submit" title="Clear input area" onclick="clearta(new Array('ta')); return false;" >CL</a>
    <a class="ui-btn ui-btn-inline" type="submit" title="Clear all" onclick="clearall(new Array('ta')); return false;" >CA</a>
</div>
</div><!-- /header -->

<div data-role="panel"  data-position="left" data-position-fixed="false" data-display="overlay" id="panel">
  
<ul data-role="listview" style="margin-top:-16px;" class="nav-search">
  <li data-icon="delete" style="background-color:#111;">
    <a href="#" data-rel="close">Close menu</a>
  </li>
<%	
    UserService userService = UserServiceFactory.getUserService();
	if (userService.getCurrentUser() != null) {
		User user = userService.getCurrentUser();
		if (user != null) {
%>  
  <li>
    <a href="<%=userService.createLogoutURL(request.getRequestURL().toString())%>" rel="external">Logout</a>
  </li>
  <li>
    <a href="#" title="Definition of a $-variable in the input area" onclick="getResult(new Array('ta'),'Definition(ta)','',''); return false;">Def</a>
  </li>
  <li>
    <a href="#"  title="List all persisted '$'-user variables" onclick="getResult(new Array('ta'),'UserVariables(ta)','',''); return false;">Vars</a>
  </li>
<%
	}
	} else {
%>
  <li>
    <a href="<%=userService.createLoginURL(request.getRequestURL().toString())%>" rel="external">Login</a>
  </li>
<%
	}
%>
  <li>
    <a href="#" title="Create a HTTP link from the input expression" onclick="linkta(); return true;">Link</a>
  </li>
  <li>
    <a href="#" onclick="help(); return true;">Help</a>
  </li>
  <li>
    <a href="http://bitbucket.org/axelclk/symja_android_library" rel="external">Project</a>
  </li>
</ul>
</div>

<div style="margin: 1px !important; padding: 0 !important;" class="ui-content" role="main">
<form autocomplete="off" action="">
<%
	// input field 
	String ci = request.getParameter("ci");
	if (ci == null) {
		ci = "ta:Input a math expression:t";
	}

	StringBuilder buf = new StringBuilder(1024);
	StringBuilder arrayString = new StringBuilder(128);
	String[] inps = ci.split("\\|");
    String[] comps;
	    
	  for (int i = 0; i < inps.length; i++) {
		comps = inps[i].split("\\:");
		if (comps.length >= 2) {
			arrayString.append("'");
			arrayString.append(comps[0]);
			if (i < inps.length - 1) {
				arrayString.append("',");
			} else {
				arrayString.append("'");
			}
			if (comps.length == 2 || comps[2].equalsIgnoreCase("i")) {
				buf.append("\n<label for=\"");
				buf.append(comps[0]);
				buf.append("\">");
				buf.append(comps[1]);
				buf.append(":</label>");
				buf.append("<input id=\"");
				buf.append(comps[0]);
				buf.append("\" size=\"25\" ");
				if (comps.length > 3) {
					buf.append("value=\"");
					buf.append(comps[3]);
					buf.append("\"");
				}
				buf.append(" />");
				// buf.append("");
				continue;
			}
			if (comps.length > 2) {
				if (comps[2].equalsIgnoreCase("t")) {
					//buf.append("\n<tr><td colspan=\"2\">");
					buf.append("<textarea id=\"");
					buf.append(comps[0]);
					buf.append("\" cols=\"40\" rows=\"3\" "); 
					buf.append("placeholder=\"Input a math expression\"");
					buf.append(">");
					if (comps.length > 3) {
						buf.append(comps[3]);
					}
					buf.append("</textarea>");
				}
			}
		}
	} 
	
	out.println(buf.toString());
%>  

<div id="result">Result output area</div>
<div id="console"></div>
 
</form>
</div>
</div>

<script type="text/javascript">
  window.___gcfg = {lang: 'en-GB'};

  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/plusone.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>

</body>
</html>