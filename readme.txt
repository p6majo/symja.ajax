==========================================
  SymjaWeb - Online Symbolic Math System
========================================== 

SymjaWeb is a web application based on Google App Engine that executes and 
displays the results of Symja expressions:
* http://symjaweb.appspot.com/ (Mobile web interface)
* http://symjaweb.appspot.com/new.jsp (Notebook interface)

See the "Symja getting started" document:
* https://bitbucket.org/axelclk/symja_android_library/wiki/Functions

See the Wiki pages:
* https://bitbucket.org/axelclk/symja_android_library/wiki
	
axelclk_AT_gmail_DOT_com 